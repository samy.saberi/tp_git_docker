FROM php:8.0-apache

RUN docker-php-ext-install pdo mysqli pdo_mysql 

RUN apt-get update -y && apt upgrade -y && apt-get install -y zip unzip memcached


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN sed -i 's/html/html\/public/' /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite