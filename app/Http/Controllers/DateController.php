<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class DateController extends Controller
{

    public function __invoke()
    {
        return new JsonResponse([
            'time' => time()
        ]);
    }
}
